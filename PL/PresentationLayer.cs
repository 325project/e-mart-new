﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PL.Menus;
using Common;
using BL;

namespace PL
{
    public class PresentationLayer
    {
        IBL bl = new BuisnessLogic();

        public PresentationLayer() 
        {
            new GreetingMenu(bl).start();
        }
    }
}
