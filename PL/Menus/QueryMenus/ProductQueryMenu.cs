﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL;

namespace PL.Menus.QueryMenus
{
    class ProductQueryMenu : Menu
    {
        IBL bl;
        public ProductQueryMenu(IBL bl) 
        {
            this.bl = bl;
        }

        override
        public Dictionary<string, MenuAction> getMenu()
        {
            Dictionary<string, MenuAction> menu = new Dictionary<string, MenuAction>();
            menu.Add("by ID", byID);
            menu.Add("by name", byName);
            menu.Add("by type", byType);
            menu.Add("by department ID", byDepartmentID);
            menu.Add("by stock count", byStockCount);
            menu.Add("by date range", byDateRange);
            return menu;
        }

        void byID()
        {
        }

        void byName()
        {
        }

        void byType()
        {
        }

        void byDepartmentID()
        {
        }

        void byStockCount()
        {
        }

        void byDateRange()
        {
        }
    }
}
