﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL;

namespace PL.Menus.QueryMenus
{
    class DepartmentQueryMenu : Menu
    {
        IBL bl;
        public DepartmentQueryMenu(IBL bl) 
        {
            this.bl = bl;
        }

        override
        public Dictionary<string, MenuAction> getMenu()
        {
            Dictionary<string, MenuAction> menu = new Dictionary<string, MenuAction>();
            menu.Add("by ID", byID);
            menu.Add("by name", byName);
            return menu;
        }

        void byID()
        {
        }

        void byName()
        {
        }
    }
}
