﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using BL;

namespace PL.Menus.QueryMenus
{
    class EmployeeQueryMenu : Menu
    {
        IBL bl;
        public EmployeeQueryMenu(IBL bl) 
        {
            this.bl = bl;
        }

        override
        public Dictionary<string, MenuAction> getMenu()
        {
            Dictionary<string, MenuAction> menu = new Dictionary<string, MenuAction>();
            menu.Add("by ID", byID);
            menu.Add("by name", byName);
            menu.Add("by supervisor ID", bySupervisorID);
            menu.Add("by department ID", byDepartmentID);
            menu.Add("by salary range", bySalaryRange);
            return menu;
        }

        void byID()
        {
            
        }

        void byName()
        {
        }

        void bySupervisorID()
        {
        }

        void byDepartmentID()
        {
        }

        void bySalaryRange()
        {
        }
    }
}
