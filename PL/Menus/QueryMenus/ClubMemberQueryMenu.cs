﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL;

namespace PL.Menus.QueryMenus
{
    class ClubMemberQueryMenu : Menu
    {
        IBL bl;
        public ClubMemberQueryMenu(IBL bl) 
        {
            this.bl = bl;
        }

        override
        public Dictionary<string, MenuAction> getMenu()
        {
            Dictionary<string, MenuAction> menu = new Dictionary<string, MenuAction>();
            menu.Add("by ID", byID);
            menu.Add("by name", byName);
            menu.Add("by date range", byDateRange);
            return menu;
        }

        void byID()
        {

        }

        void byName()
        {
        }

        void byDateRange()
        {
        }
    }
}
