﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using BL;

namespace PL.Menus
{
    class NewTransactionMenu : Menu
    {
        IBL bl;
        Transaction t;
        public NewTransactionMenu(IBL bl) 
        {
            this.bl = bl;
            t = new Transaction();
        }

        override
        public Dictionary<string, MenuAction> getMenu()
        {
            Dictionary<string, MenuAction> menu = new Dictionary<string, MenuAction>();

            menu.Add("edit transaction", editTransactionAction);
            menu.Add("edit receipts", editReceiptsAction);
            
            return menu;
        }

        void editTransactionAction()
        {
            new Editor<Transaction>(t).start();
        }

        void editReceiptsAction()
        {
            t.receipts = new List<Receipt>();
            Lister<Receipt> lister = new Lister<Receipt>(t.receipts);
            lister.setAddAction(add);
            lister.setEditAction(edit);
            lister.setRemoveAction(remove);
            lister.setMoreInfo(info);
        }

        Receipt add(Receipt item, int index, List<Receipt> l)
        {
            Receipt r = new Editor<Receipt>().start();
            return r;
        }

        Receipt edit(Receipt item, int index, List<Receipt> l)
        {
            Receipt r = new Editor<Receipt>(item).start();
            return item;
        }

        void remove(Receipt item, int index, List<Receipt> l)
        {
            l.RemoveAt(index);
        }

        void info(Receipt item, int index, List<Receipt> l)
        {
            
        }
    }
}
