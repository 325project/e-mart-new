﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using BL;

namespace PL.Menus
{
    class MainMenu : Menu
    {
        IBL bl;
        public MainMenu(IBL bl) 
        {
            this.bl = bl;
        }

        override
        public Dictionary<string, MenuAction> getMenu()
        {
            Dictionary<string, MenuAction> menu = new Dictionary<string, MenuAction>();

            if (bl.getCurrentUser() is Employee)
            {
                menu.Add("new Transaction", newTransactionAction);
                menu.Add("new Club member", newClubMemberAction);
                menu.Add("query", queryAction);
            }
            else if (bl.getCurrentUser() is Supervisor)
            {
                menu.Add("new Transaction", newTransactionAction);
                menu.Add("new Club member", newClubMemberAction);
                menu.Add("manage employees", manageEmployeesAction);
                menu.Add("manage data", manageDataAction);
                menu.Add("query", queryAction);
            }

            
            return menu;
        }

        void newTransactionAction()
        {
            new NewTransactionMenu(bl).start();
        }

        void newClubMemberAction()
        {
            new Editor<ClubMember>().start();
        }

        void manageEmployeesAction()
        {
            new ManageEmployeesMenu(bl).start();
        }

        void manageDataAction()
        {
            new ManageDataMenu(bl).start();
        }

        void queryAction()
        {
            new QuerySelectorMenu(bl).start();
        }
    }
}
