﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL;
using Common;
using PL.Menus.MagnageMenus;

namespace PL.Menus
{
    class ManageDataMenu : Menu
    {
        IBL bl;
        public ManageDataMenu(IBL bl) 
        {
            this.bl = bl;
        }

        override
        public Dictionary<string, MenuAction> getMenu()
        {
            Dictionary<string, MenuAction> menu = new Dictionary<string, MenuAction>();

            menu.Add("manage products", manageProductsAction);
            menu.Add("manage club members", manageClubMembersAction);
            menu.Add("manage departments", manageDepartmentsAction);
            
            return menu;
        }

        private void manageClubMembersAction()
        {
            new ManageClubMembersMenu(bl).start();
        }

        private void manageProductsAction()
        {
            new ManageProductsMenu(bl).start();
        }

        private void manageDepartmentsAction()
        {
            new ManageDepartmentsMenu(bl).start();
        }
    }
}
