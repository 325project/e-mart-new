﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using BL;

namespace PL.Menus.MagnageMenus
{
    class ManageDepartmentsMenu : Menu
    {
        IBL bl;
        public ManageDepartmentsMenu(IBL bl) 
        {
            this.bl = bl;
        }

        override
        public Dictionary<string, MenuAction> getMenu()
        {
            Dictionary<string, MenuAction> menu = new Dictionary<string, MenuAction>();

            menu.Add("add", addAction);
            menu.Add("remove", removeAction);
            menu.Add("update", updateAction);
            menu.Add("list", listAction);
            
            return menu;
        }

        void addAction()
        {
        }

        void removeAction()
        {
        }

        void updateAction()
        {
        }

        void listAction()
        {
            new Lister<Department>(bl.listDepartments());
        }
    }
}
