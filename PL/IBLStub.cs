﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using BL;

namespace PL
{
    class IBLStub : IBL
    {
        public User getCurrentUser()
        {
            throw new NotImplementedException();
        }

        public Employee employeeLogin(string username, string password)
        {
            throw new NotImplementedException();
        }

        public Employee employeeById(string ID)
        {
            throw new NotImplementedException();
        }

        public Employee employeeSignup(Employee u)
        {
            throw new NotImplementedException();
        }

        public List<Employee> allEmployees()
        {
            throw new NotImplementedException();
        }

        public Supervisor supervisorLogin(string username, string password)
        {
            throw new NotImplementedException();
        }

        public Supervisor supervisorById(string ID)
        {
            throw new NotImplementedException();
        }

        public Supervisor supervisorSignup(Supervisor u)
        {
            throw new NotImplementedException();
        }

        public List<Employee> employeeByName(string fname, string lname)
        {
            throw new NotImplementedException();
        }

        public ClubMember addClubMember(ClubMember c)
        {
            throw new NotImplementedException();
        }

        public Department addDepartment(Department p)
        {
            throw new NotImplementedException();
        }

        public Product addProduct(Product p)
        {
            throw new NotImplementedException();
        }

        public List<ClubMember> listClubMembers()
        {
            throw new NotImplementedException();
        }

        public List<Department> listDepartments()
        {
            throw new NotImplementedException();
        }

        public List<Product> listProducts()
        {
            throw new NotImplementedException();
        }

        public ClubMember removeClubMember(string id)
        {
            throw new NotImplementedException();
        }

        public Department removeDepartment(string id)
        {
            throw new NotImplementedException();
        }

        public Product removeProduct(string id)
        {
            throw new NotImplementedException();
        }

        public ClubMember updateClubMember(ClubMember c)
        {
            throw new NotImplementedException();
        }

        public Department updateDepartment(Department p)
        {
            throw new NotImplementedException();
        }

        public Product updateProduct(Product p)
        {
            throw new NotImplementedException();
        }


        public Employee addEmployee(Employee p)
        {
            throw new NotImplementedException();
        }

        public List<Employee> listEmployees()
        {
            throw new NotImplementedException();
        }

        public Employee removeEmployee(string id)
        {
            throw new NotImplementedException();
        }

        public Employee updateEmployee(Employee p)
        {
            throw new NotImplementedException();
        }


        public List<ClubMember> clubmemberByDateRange(DateTime min, DateTime max)
        {
            throw new NotImplementedException();
        }

        public ClubMember clubmemberByID(string id)
        {
            throw new NotImplementedException();
        }

        public List<ClubMember> clubmemberByName(string fname, string lname)
        {
            throw new NotImplementedException();
        }

        public Department departmentByID(string id)
        {
            throw new NotImplementedException();
        }

        public List<Department> departmentByName(string name)
        {
            throw new NotImplementedException();
        }

        public List<Employee> employeeByDepartmentID(string dID)
        {
            throw new NotImplementedException();
        }

        public Employee employeeByID(string id)
        {
            throw new NotImplementedException();
        }

        public List<Employee> employeeBySalaryRange(float min, float max)
        {
            throw new NotImplementedException();
        }

        public List<Employee> employeeBySupervisorID(string sID)
        {
            throw new NotImplementedException();
        }

        public List<Product> productByDateRange(DateTime min, DateTime max)
        {
            throw new NotImplementedException();
        }

        public List<Product> productByDepartmentID(string dID)
        {
            throw new NotImplementedException();
        }

        public Product productByID(string id)
        {
            throw new NotImplementedException();
        }

        public List<Product> productByName(string name)
        {
            throw new NotImplementedException();
        }

        public List<Product> productByStockCount(string dID)
        {
            throw new NotImplementedException();
        }

        public List<Product> productByType(Product.Type type)
        {
            throw new NotImplementedException();
        }


        public List<Product> productByStockCount(int min, int max)
        {
            throw new NotImplementedException();
        }
    }
}
