﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PL
{
    abstract class Menu
    {
        const string FORMAT = "{0}. {1}";

        public delegate void MenuAction();
        public abstract Dictionary<string ,MenuAction> getMenu();

        public void start() 
        {
            while (true) {
                Console.Clear();
                Dictionary<string, MenuAction> menu = getMenu();

                Console.WriteLine(string.Format(FORMAT, 0, "< return"));
                for (int i = 0; i < menu.Count(); i++) 
                {
                    Console.WriteLine(string.Format(FORMAT, i+1, menu.ElementAt(i).Key));
                }

                try
                {
                    int ans = int.Parse(Console.ReadLine());
                    if (ans < 0 || ans > menu.Count())
                        throw new IndexOutOfRangeException("please enter a number between 0 and " + menu.Count());
                    if (ans == 0)
                    {
                        returnAction();
                        return;
                    }
                    else
                        menu.ElementAt(ans - 1).Value();
                }
                catch (FormatException e)
                {
                    Console.WriteLine("Please Enter a number");
                    Console.WriteLine("press any key to continue ...");
                    Console.ReadKey();
                }
                catch (IndexOutOfRangeException e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("press any key to continue ...");
                    Console.ReadKey();
                }
            }
        }

        private void returnAction()
        {
        }
    }
}
