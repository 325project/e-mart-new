﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.ComponentModel.DataAnnotations;
using Common;

namespace PL
{
    class Editor<T> where T : new()
    {
        private T obj;
        const string EXIT_WORD = "exit";
        const string SAVE_WORD = "save";
        const string FORMAT = "{0}: {1}";
        List<FieldInfo> fields;

        public Editor()
        {
            this.obj = new T();
        }

        public Editor(T obj)
        {
            this.obj = obj;
        }

        public T start()
        {
            fields = typeof(T)
                .GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)
                .Where(prop => prop.IsDefined(typeof(DisplayAttribute)))
                .ToList();
            while (true) 
            {
                update();
                string input = Console.ReadLine();
                if (input == EXIT_WORD)
                    return default(T);
                
                try
                {
                    if (input == SAVE_WORD) 
                    {
                        validate();
                        return obj;
                    }
                    parseInput(input);
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Press any key to continue ...");
                    Console.ReadKey();
                }
            }
        }

        private void validate()
        {
            foreach(FieldInfo f in fields)
                if (f.IsDefined(typeof(RequiredAttribute)) && (f.GetValue(obj) == null || f.GetValue(obj).ToString() == ""))
                    throw new MissingFieldException(string.Format("please provide required field: `{0}`" , ((DisplayAttribute)f.GetCustomAttribute(typeof(DisplayAttribute))).Name));
        }

        private void update()
        {
            
            Console.Clear();
            foreach (FieldInfo f in fields)
            {
                DisplayAttribute dipslay = (DisplayAttribute)f.GetCustomAttribute(typeof(DisplayAttribute));
                string val = dipslay.Name;
                if(f.IsDefined(typeof(RequiredAttribute))) val += " (required)";
                Console.WriteLine(string.Format(FORMAT, val, f.GetValue(obj)));
            }
                
        }

        private void parseInput(string input) 
        {
            foreach (string term in input.Trim().Split(',')) 
            {
                string[] tuple = term.Trim().Split('=');
                if (tuple.Length != 2)
                    throw new FormatException("incorrect format ! please input `field1 = value1 , field2 = value2 ...`");
                string key = tuple[0].Trim();
                string val = tuple[1].Trim();

                var query = from f in fields
                            where ((DisplayAttribute)f.GetCustomAttribute(typeof(DisplayAttribute))).Name == key
                            select f;

                if (query.Count() < 1)
                    throw new Exception(string.Format("field `{0}` not found",key));

                var v = Convert.ChangeType(val, query.ElementAt(0).FieldType);
                query.ElementAt(0).SetValue(obj, v);
            }
        }
        
    }
}
