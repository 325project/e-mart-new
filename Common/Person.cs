﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Common
{
    [Serializable]
    public class Person
    {
        public enum Gender { Male , Female , Other }

        [Required]
        [Display(Name = "ID")]
        public string ID;

        [Required]
        [Display(Name = "first name")]
        public string fname;

        [Required]
        [Display(Name = "last name")]
        public string lname;

        [Display(Name = "gender")]
        public Gender gender = Gender.Male;
    }
}
