﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    [Serializable]
    public class Department
    {
        [Required]
        [Display(Name = "ID")]
        public string ID;

        [Required]
        [Display(Name = "name")]
        public string name;
    }
}
