﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    [Serializable]
    public class ClubMember : Person
    {
        [Required]
        [Display(Name = "memeber ID")]
        public string memeberID;

        List<string> transactionHistory;

        [Required]
        [Display(Name = "date of birth")]
        DateTime dateOfBirth;
    }
}
