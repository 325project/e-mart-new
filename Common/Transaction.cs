﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Common
{
    [Serializable]
    public class Receipt
    {
        public Product product;
        public int ammout;
        public float price;
    }

    [Serializable]
    public class Transaction
    {
        public enum PaymentMethod { Credit , Cash }

        [Required]
        [Display(Name = "ID")]
        public string ID;

        [Display(Name = "create date")]
        public DateTime createDate;

        [Display(Name = "is a return")]
        public bool isAReturn;

        public List<Receipt> receipts;

        [Required]
        [Display(Name = "payment method")]
        public PaymentMethod paymentMethod;
    }
}
