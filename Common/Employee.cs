﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    [Serializable]
    public class Employee : User
    {
        [Required]
        [Display(Name = "supervisor ID")]
        public string supervisorID;

        [Required]
        [Display(Name = "department ID")]
        public string departmentID;

        [Required]
        [Display(Name = "salary")]
        public float salary;
    }
}
