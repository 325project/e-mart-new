﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using Common;

namespace BL
{
    public class BuisnessLogic : IBL
    {
        User currentUser;

        IDAL dal;
        public BuisnessLogic() 
        {
            dal = new DataAccessLayer();
        }  

        #region session

        public User getCurrentUser()
        {
            return currentUser;
        }

        public Employee employeeLogin(string username, string password)
        {
            var query = from e in dal.getOp<Employee>().getList()
                        where e.isCredsCorrect(username, password)
                        select e;
            if (query.Count() < 1)
                return null;
            if (query.Count() > 1)
                throw new FatalException(string.Format("more than one Employee with (username: {0} , password: {1})", username, password));

            currentUser = query.ElementAt(0);
            return query.ElementAt(0);
        }

        public Employee employeeSignup(Employee u)
        {
            if (employeeLogin(u.username, u.password) != null)
                throw new Exception("Employee already exists");
            if (employeeById(u.ID) != null)
                throw new Exception("Employee already exists");
            if (supervisorById(u.supervisorID) == null)
                throw new Exception("Supervisor does not exists");

            IDBOperation<Employee> op = dal.getOp<Employee>();
            op.getList().Add(u);
            op.save();
            return u;
        }

        public Supervisor supervisorLogin(string username, string password)
        {
            var query = from e in dal.getOp<Supervisor>().getList()
                        where e.isCredsCorrect(username, password)
                        select e;
            if (query.Count() < 1)
                return null;
            if (query.Count() > 1)
                throw new FatalException(string.Format("more than one Employee with (username: {0} , password: {1})", username, password));
            currentUser = query.ElementAt(0);
            return query.ElementAt(0);
        }

        public Supervisor supervisorSignup(Supervisor s)
        {
            if (supervisorLogin(s.username, s.password) != null)
                throw new Exception("Employee already exists");
            if (supervisorById(s.ID) != null)
                throw new Exception("Employee already exists");

            IDBOperation<Supervisor> op = dal.getOp<Supervisor>();
            op.getList().Add(s);
            op.save();
            return s;
        }

        public Employee employeeById(string ID)
        {
            var query = from e in dal.getOp<Employee>().getList()
                        where e.ID == ID
                        select e;
            if (query.Count() < 1)
                return null;
            if (query.Count() > 1)
                throw new FatalException(string.Format("more than one Employee with (ID: {0})", ID));

            return query.ElementAt(0);
        }

        public Supervisor supervisorById(string ID)
        {
            var query = from e in dal.getOp<Supervisor>().getList()
                        where e.ID == ID
                        select e;
            if (query.Count() < 1)
                return null;
            if (query.Count() > 1)
                throw new FatalException(string.Format("more than one Employee with (ID: {0})", ID));

            return query.ElementAt(0);
        }

        public List<Employee> allEmployees()
        {
            return dal.getOp<Employee>().getList();
        }
        #endregion

        #region manage data

        public Product addProduct(Product p)
        {
            throw new NotImplementedException();
        }

        public Product removeProduct(string id)
        {
            throw new NotImplementedException();
        }

        public Product updateProduct(Product p)
        {
            throw new NotImplementedException();
        }

        public List<Product> listProducts()
        {
            return dal.getOp<Product>().getList();
        }

        public ClubMember addClubMember(ClubMember c)
        {
            throw new NotImplementedException();
        }

        public ClubMember removeClubMember(string id)
        {
            throw new NotImplementedException();
        }

        public ClubMember updateClubMember(ClubMember c)
        {
            throw new NotImplementedException();
        }

        public List<ClubMember> listClubMembers()
        {
            return dal.getOp<ClubMember>().getList();
        }

        public Department addDepartment(Department p)
        {
            throw new NotImplementedException();
        }

        public Department removeDepartment(string id)
        {
            throw new NotImplementedException();
        }

        public Department updateDepartment(Department p)
        {
            throw new NotImplementedException();
        }

        public List<Department> listDepartments()
        {
            return dal.getOp<Department>().getList();
        }
        #endregion

        #region manage employees

        public Employee addEmployee(Employee p)
        {
            throw new NotImplementedException();
        }

        public Employee removeEmployee(string id)
        {
            throw new NotImplementedException();
        }

        public Employee updateEmployee(Employee p)
        {
            throw new NotImplementedException();
        }

        public List<Employee> listEmployees()
        {
            return dal.getOp<Employee>().getList();
        }

        #endregion

        #region queries
        public Employee employeeByID(string id)
        {
            throw new NotImplementedException();
        }

        public List<Employee> employeeByName(string fname, string lname)
        {
            throw new NotImplementedException();
        }

        public List<Employee> employeeBySupervisorID(string sID)
        {
            throw new NotImplementedException();
        }

        public List<Employee> employeeByDepartmentID(string dID)
        {
            throw new NotImplementedException();
        }

        public List<Employee> employeeBySalaryRange(float min, float max)
        {
            throw new NotImplementedException();
        }

        public Product productByID(string id)
        {
            throw new NotImplementedException();
        }

        public List<Product> productByName(string name)
        {
            throw new NotImplementedException();
        }

        public List<Product> productByType(Product.Type type)
        {
            throw new NotImplementedException();
        }

        public List<Product> productByDepartmentID(string dID)
        {
            throw new NotImplementedException();
        }

        public List<Product> productByStockCount(int min , int max)
        {
            throw new NotImplementedException();
        }

        public List<Product> productByDateRange(DateTime min, DateTime max)
        {
            throw new NotImplementedException();
        }

        public ClubMember clubmemberByID(string id)
        {
            throw new NotImplementedException();
        }

        public List<ClubMember> clubmemberByName(string fname, string lname)
        {
            throw new NotImplementedException();
        }

        public List<ClubMember> clubmemberByDateRange(DateTime min, DateTime max)
        {
            throw new NotImplementedException();
        }

        public Department departmentByID(string id)
        {
            throw new NotImplementedException();
        }

        public List<Department> departmentByName(string name)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
