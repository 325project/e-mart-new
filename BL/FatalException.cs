﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    class FatalException : Exception
    {
        public FatalException(string message)
            : base(message)
        {
        }
    }
}
