﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DataAccessLayer : IDAL
    {
        public IDBOperation<T> getOp<T>()
        {
            return new DBOperation<T>();
        }

        public IDBOperation<T> getOp<T>(List<T> list)
        {
            return new DBOperation<T>(list);
        }
    }
}
