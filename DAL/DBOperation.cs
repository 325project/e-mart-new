﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DAL
{
    // a GENERIC class for interacting with the database using a List.
    public class DBOperation<T> : IDBOperation<T>
    {
        // encryption key for encrypting the data.
        static byte[] ENCRYPTION_KEY_BYTES = Convert.FromBase64String(Constants.ENCRYPTION_KEY);
        // the pat of the file.
        string filePath;
        // the list of objects to retreive or store , it is public so anyone can edit it.
        public List<T> list = null;

        public DBOperation()
        {
            init();
            retreive();
        }

        public DBOperation(List<T> newList)
        {
            init();
            list = newList;
        }

        void init()
        {
            // if T is not serilizable throw exception.
            if (!typeof(T).IsSerializable && !(typeof(ISerializable).IsAssignableFrom(typeof(T))))
                throw new InvalidOperationException("A serializable Type is required");

            //the file path is the name of the full name of the class , using reflection.
            filePath = Constants.ROOT_DIRECTORY + typeof(T).FullName + ".xml";

            //create the file and directories if they do not exist.
            if (!File.Exists(filePath))
            {
                Directory.CreateDirectory(Constants.ROOT_DIRECTORY);
                File.Create(filePath);
            }
        }


        //serialize then encrypt the save the list to the file
        public IDBOperation<T> save()
        {
            File.WriteAllText(filePath, Encrypt(ToString(list)));
            return this;
        }

        //read the file , decrypt and deserialize it , then set the list.
        public IDBOperation<T> retreive()
        {
            string txt = File.ReadAllText(filePath);
            if (txt == String.Empty)
                list = new List<T>();
            else
                list = FromString(Decrypt(txt));
            return this;
        }

        // converts the list to a (base64) string.
        private string ToString(List<T> obj)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                new BinaryFormatter().Serialize(ms, obj);
                return Convert.ToBase64String(ms.ToArray());
            }

            var stringwriter = new System.IO.StringWriter();
            var serializer = new XmlSerializer(typeof(List<T>));
            serializer.Serialize(stringwriter, obj);
            return stringwriter.ToString();
        }

        //convert the (base64) string to a list.
        private List<T> FromString(string xmlText)
        {
            byte[] bytes = Convert.FromBase64String(xmlText);
            using (MemoryStream ms = new MemoryStream(bytes, 0, bytes.Length))
            {
                ms.Write(bytes, 0, bytes.Length);
                ms.Position = 0;
                return (List<T>)new BinaryFormatter().Deserialize(ms);
            }

            var stringReader = new System.IO.StringReader(xmlText);
            var serializer = new XmlSerializer(typeof(T));
            return (List<T>)serializer.Deserialize(stringReader);
        }

        //garble up the string using the encryption key.
        private string Encrypt(string s)
        {
            if (String.IsNullOrEmpty(s))
            {
                throw new ArgumentNullException
                       ("The string which needs to be encrypted can not be null.");
            }
            DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream,
                cryptoProvider.CreateEncryptor(ENCRYPTION_KEY_BYTES, ENCRYPTION_KEY_BYTES), CryptoStreamMode.Write);
            StreamWriter writer = new StreamWriter(cryptoStream);
            writer.Write(s);
            writer.Flush();
            cryptoStream.FlushFinalBlock();
            writer.Flush();
            return Convert.ToBase64String(memoryStream.GetBuffer(), 0, (int)memoryStream.Length);
        }

        //de-garble up the string. 
        private string Decrypt(string s)
        {
            if (String.IsNullOrEmpty(s))
            {
                throw new ArgumentNullException
                   ("The string which needs to be decrypted can not be null.");
            }
            DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();
            MemoryStream memoryStream = new MemoryStream
                    (Convert.FromBase64String(s));
            CryptoStream cryptoStream = new CryptoStream(memoryStream,
                cryptoProvider.CreateDecryptor(ENCRYPTION_KEY_BYTES, ENCRYPTION_KEY_BYTES), CryptoStreamMode.Read);
            StreamReader reader = new StreamReader(cryptoStream);
            return reader.ReadToEnd();
        }

        public List<T> getList() 
        {
            return list;
        }
    }
}
