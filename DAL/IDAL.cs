﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public interface IDAL
    {
        IDBOperation<T> getOp<T>();
        IDBOperation<T> getOp<T>(List<T> list);
    }
}
